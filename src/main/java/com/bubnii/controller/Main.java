package com.bubnii.controller;

/**
 * @author Serhii Bubnii
 * @version 1.0
 * @since 30.03.2019
 */
public final class Main {
    /**
     * This is a utility class so
     * a constructor should be private.
     */
    private Main() {
    }

    /**
     * This is the main method which starts the program.
     *
     * @param args Unused.
     */
    public static void main(final String[] args) {
        new Manager().chooseActions();
    }
}

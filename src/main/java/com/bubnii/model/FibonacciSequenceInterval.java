package com.bubnii.model;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Serhii Bubnii
 * @version 1.0
 * @since 30.03.2019
 */
public class FibonacciSequenceInterval {

    private static final int HANDRED_PERCENT = 100;
    /**
     * Initialization first three Fibonacci Numbers
     */
    private static int firstNumberFibonacci = 0;
    private static int secondNumberFibonacci = 1;
    private static int thirdNumberFibonacci = 1;
    /**
     * Initialize the list for even and odd numbers
     */
    private static List<Integer> arrayOddFibonacci = new LinkedList<Integer>();
    private static List<Integer> arrayEvenFibonacci = new LinkedList<Integer>();
    private static List<Integer> arrayFibonacci = new LinkedList<Integer>();
    /**
     * DecimalFormat is required for formatted output of float-point numbers
     */
    private static DecimalFormat df = new DecimalFormat("#.##");

    /**
     * @return list with Fibonacci numbers
     */
    public static List<Integer> getArrayFibonacci() {
        return arrayFibonacci;
    }

    /**
     * @return list with odd Fibonacci numbers
     */
    public static List<Integer> getArrayOddFibonaccis() {
        return arrayOddFibonacci;
    }

    /**
     * @return list with even Fibonacci numbers
     */
    public static List<Integer> getArrayEvenFibonacci() {
        return arrayEvenFibonacci;
    }

    /**
     * This method is used to find the sequence of odd numbers of fibonacci
     * in an interval [startInterval, finishInterval]
     *
     * @param startInterval  from this parameter begins the interval
     *                       of Fibonacci numbers
     * @param finishInterval this parameter completes the interval
     *                       of Fibonacci numbers
     */
    public static void calcSequence(
            final int startInterval, final int finishInterval) {
        while (firstNumberFibonacci <= finishInterval) {
            if (firstNumberFibonacci >= startInterval) {
                if (firstNumberFibonacci % 2 != 0) {
                    arrayOddFibonacci.add(firstNumberFibonacci);
                } else {
                    arrayEvenFibonacci.add(firstNumberFibonacci);
                }
            }
            swap();
        }
    }

    /**
     * This method rebuilds the even Fibonacci numbers in reverse order
     */
    public static void reverseArrayOdd() {
        Collections.reverse(arrayOddFibonacci);
    }

    /**
     * This method rebuilds the odd Fibonacci numbers in reverse order
     */
    public static void reverseArrayEven() {
        Collections.reverse(arrayEvenFibonacci);
    }

    /**
     * This method calculates the sum in a series of fibonacci
     *
     * @return sum of Fibonacci numbers
     */
    public static int sumOddEvenNumber() {
        int sumOddFibonacci = 0;
        int sumEvenFibonacci = 0;
        for (int iOdd : arrayOddFibonacci) {
            sumOddFibonacci += iOdd;
        }

        for (int iEven : arrayEvenFibonacci) {
            sumEvenFibonacci += iEven;
        }
        return sumOddFibonacci + sumEvenFibonacci;
    }

    /**
     * This method builds a series of Fibonacci
     *
     * @param sizeSet the number of elements in the Fibonacci series
     */
    public static void calculationFibonacciSet(final int sizeSet) {
        for (int index = 1; index <= sizeSet; ++index) {
            arrayFibonacci.add(firstNumberFibonacci);
            swap();
        }
    }

    /**
     * This method changes three variables
     */
    private static void swap() {
        firstNumberFibonacci = secondNumberFibonacci;
        secondNumberFibonacci = thirdNumberFibonacci;
        thirdNumberFibonacci = firstNumberFibonacci + secondNumberFibonacci;
    }

    /**
     * This metod calculaters the percentage of
     * the occurrence of even and odd numbers
     */
    public static void percentageOddEvenFibonacciNumber() {
        int counterOdd = 0;
        int counterEven = 0;
        for (int i : arrayFibonacci) {
            if (i % 2 == 0) {
                counterEven++;
            } else {
                counterOdd++;
            }
        }
        double interestOdd = getInterest(counterOdd, counterEven);
        double interestEven = getInterest(counterEven, counterOdd);

        System.out.println("Percentage of odd = "
                + df.format(interestOdd)
                + "% and even = " + df.format(interestEven)
                + "% Fibonacci numbers");
    }

    /**
     * @param counterOdd  counter of odd numbers in the Fibonacci sequence
     * @param counterEven counter of even numbers in the Fibonacci sequence
     * @return percentage of odd or even Fibonacci numbers
     */

    private static double getInterest(
            final double counterOdd, final double counterEven) {
        return (HANDRED_PERCENT / (counterOdd + counterEven)) * counterOdd;
    }

}

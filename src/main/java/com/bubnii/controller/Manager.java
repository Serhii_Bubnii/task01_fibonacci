package com.bubnii.controller;

import com.bubnii.model.FibonacciSequenceInterval;

import java.util.Scanner;

/**
 * @author Serhii Bubnii
 * @version 1.0
 * @since 31.03.2019
 */
class Manager {
    /**
     * To read the number from the console
     */
    private Scanner scanner = new Scanner(System.in);

    /**
     * This method allows you to select functions
     */
    void chooseActions() {
        System.out.println("Enter 1 to print odd numbers and reverse order");
        System.out.println("Enter 2 to print even numbers and reverse order");
        System.out.println("Enter 3 to print the sum of Fibonacci interval");
        System.out.println("Enter 4 to build Fibonacci numbers");
        System.out.println("Enter 5 to get percentage of "
                + "odd and even Fibonacci numbers");
        System.out.println("Press 0 to exit");

        int terminalNumber = scanner.nextInt();

        switch (terminalNumber) {
            case 0:
                System.exit(0);
                break;
            case 1:
                new Manager().printOddNumber();
                break;
            case 2:
                new Manager().printEvenNumber();
                break;
            case 3:
                new Manager().printSum();
                break;
            case 4:
                new Manager().buildFibonacciNumbers();
                break;
            case 5:
                new Manager().percentageOddEven();
                break;
            default:
                System.out.println("Incorrect number!!!");
        }
    }

    /**
     * This method prints odd Fibonacci numbers and reverse order
     */
    private void printOddNumber() {
        System.out.print("Enter the starting interval Fibonacci numbers: ");
        int startInterval = scanner.nextInt();
        System.out.print("Enter the end of the interval of Fibonacci numbers:");
        int finishInterval = scanner.nextInt();
        FibonacciSequenceInterval
                .calcSequence(startInterval, finishInterval);
        System.out.print("An array of odd Fibonacci numbers");
        System.out.println(FibonacciSequenceInterval.getArrayOddFibonaccis());
        FibonacciSequenceInterval.reverseArrayOdd();
        System.out.println("Array Fibonacci in reverse order: "
                + FibonacciSequenceInterval.getArrayOddFibonaccis());
    }

    /**
     * This method prints even Fibonacci numbers and reverse order
     */
    private void printEvenNumber() {
        System.out.print("Enter the starting interval Fibonacci numbers: ");
        int startInterval = scanner.nextInt();
        System.out.print("Enter the end of the interval of Fibonacci numbers:");
        int finishInterval = scanner.nextInt();
        FibonacciSequenceInterval
                .calcSequence(startInterval, finishInterval);
        System.out.print("An array of even Fibonacci numbers");
        System.out.println(FibonacciSequenceInterval.getArrayEvenFibonacci());
        FibonacciSequenceInterval.reverseArrayEven();
        System.out.println("Array Fibonacci in reverse order: "
                + FibonacciSequenceInterval.getArrayEvenFibonacci());
    }

    /**
     * This method prints the sum odd and even Fibonacci numbers
     */
    private void printSum() {
        System.out.print("Enter the starting interval Fibonacci numbers: ");
        int startInterval = scanner.nextInt();
        System.out.print("Enter the end of the interval of Fibonacci numbers:");
        int finishInterval = scanner.nextInt();
        FibonacciSequenceInterval
                .calcSequence(startInterval, finishInterval);
        System.out.println("Sum of Fibonacci numbers in the interval ["
                + startInterval + ":" + finishInterval + "]");
        System.out.println(FibonacciSequenceInterval.sumOddEvenNumber());
    }

    /**
     * This method print the with a given number of items
     */
    private void buildFibonacciNumbers() {
        System.out.print("Enter the number of elements: ");
        int countNum = scanner.nextInt();
        FibonacciSequenceInterval.calculationFibonacciSet(countNum);
        System.out.print("Fibonacci numbers : ");
        System.out.println(FibonacciSequenceInterval.getArrayFibonacci());
    }

    /**
     * This method print percentage of odd and even Fibonacci numbers
     */
    private void percentageOddEven() {
        System.out.print("Enter the number of elements: ");
        int countNum = scanner.nextInt();
        FibonacciSequenceInterval.calculationFibonacciSet(countNum);
        System.out.print("Fibonacci numbers : ");
        System.out.println(FibonacciSequenceInterval.getArrayFibonacci());
        FibonacciSequenceInterval.percentageOddEvenFibonacciNumber();
    }
}
